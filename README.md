# HOTEL-MANAGEMENT-SYSTEM
The Hotel Management System Project is a general software developed to simplify hotel operations by automating them. the project has Login System security with database in MySql. This project collects information about customers, room and other hotel services. you can add user and costomers and delete them and search user as per there name phone number.  


## Features

- User authentication: Allows users to sign up and login.
- Room management: View available rooms, book rooms, and view room details.
- Customer management: Save customer details.
- Hotel details: View hotel information.

## Tech Stacks

- Python 3.x
- Tkinter (usually comes pre-installed with Python)
- SQLite

## Setup

1. Clone the repository:

    bash
    git clone https://github.com/26thrishitha/hotel-management-system.git
    

2. Navigate to the project directory:

    bash
    cd hotel-management-system
    

3. Install dependencies (if any):

    bash
    pip install -r requirements.txt
    

4. Run the application:

    bash
    python main.py
    

## Usage

- Upon running the application, you will be prompted to either log in or sign up.
- After logging in, you will be directed to the main page where you can access various features such as room management, customer management, and hotel details.
- Follow the on-screen instructions to perform different actions within the application.

## Contributors

- [thrishitha] (https://github.com/26thrishitha)
- [sara]
- [lahari]
-[amulya]
[tejaswini]
[komali]

## Special thanks to 
charGPT
blackbox AI
youtube
google
